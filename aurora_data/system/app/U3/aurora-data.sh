#!/system/bin/sh
userdata="/system/userdata"
aurora="/data/aurora"
aurora_ex="/data/aurora/exist"

CallRe="/sdcard/通话录音"
ScrShoot="/sdcard/截屏"

song_dir="/sdcard/Music/auroramusic/song"
lrc_dir="/sdcard/Music/auroramusic/lyric"

copy=0
if [ ! -d ${aurora_ex} ]; then
        copy=1
fi

systemuserapp="/system/userapp"
userapp="/data/app"
store_copyed="/data/app/store_copyed"

md5_orignal_file="/data/aurora/md5_original.file"

while true; do


    echo "copy userapp begin"
    if [ ! -f ${store_copyed} ] ; then
        cp -rf ${systemuserapp}/Aurora_Store.apk ${userapp}
        echo "copy copyed finish"
        echo 1 > ${store_copyed}
    fi

    chown system ${userapp}/*
    chmod 666 ${userapp}/*

    echo 1.copy checking time
    tmpyear=`date +%Y`
    echo ${tmpyear}
    if [ ${tmpyear} -eq "1970" ];then
        echo "sleep 1"
        sleep 1
        continue
    fi   
   
    sdcard_ex="/sdcard/dirtest"
    if [ ! -d ${sdcard_ex} ]; then
        echo "mkdir fail.continue"
        mkdir -p ${sdcard_ex}
        sleep 1
	continue
    fi
    rm -rf ${sdcard_ex}


    if [ ! -f ${md5_orignal_file} ];then
        /system/bin/sh /system/etc/md5_original.sh
        sync
    fi

    echo 2.copy checking dir
    if [ -d ${aurora} ]; then
        if [ -d ${CallRe} ];then
           mkdir -p /sdcard/CallRecording/
           cp -rf ${CallRe}/* /sdcard/CallRecording/
           rm -rf ${CallRe}
        fi
      
        if [ -d ${ScrShoot} ];then
          mkdir -p /sdcard/Screenshots/
          cp -rf ${ScrShoot}/* /sdcard/Screenshots/
          rm -rf ${ScrShoot}
        fi
        
        if [ $copy -eq 0 ]; then
             echo "copy Done."
             break;
        fi
    fi

    mkdir ${aurora}
    mkdir -p ${song_dir}
    mkdir -p ${lrc_dir}

    cp -Rf ${userdata}/data /
    cp /system/sdcard/*.mp4 /sdcard/

    mkdir /sdcard/DCIM/
    chmod 777 /sdcard/DCIM/
    cp /system/sdcard/DCIM/* /sdcard/DCIM/

    #copy mp3 & lrc
    cp system/sdcard/music/*.mp3 ${song_dir}
    cp system/sdcard/lyric/*.lrc ${lrc_dir}
    sync
    mkdir -p ${aurora_ex}
    copy=0

done

chmod 777 /data/aurora
chmod 777 /data/aurora/change
chmod 777 /data/aurora/change/lockscreen
