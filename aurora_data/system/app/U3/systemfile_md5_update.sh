#!/bin/bash
sh /system/etc/systemfile_current_md5.sh

if [ ! -f /data/aurora/system_original_md5.file ];then
    echo "aurora file not exist."
    exit
fi
tmp_current=`md5 /data/aurora/system_original_md5.file`
currentfile_md5=${tmp_current:0:32}

tmp_original=`md5 /data/aurora/system_current_md5.file`
originalfile_md5=${tmp_original:0:32}

echo "current_system_md5:"${currentfile_md5}
echo "original_system_md5:"${originalfile_md5}

if [[ ${currentfile_md5} != ${originalfile_md5} ]];then
    echo "aurora system abnormal."
    exit 1
else
    echo "aurora system md5 file reset."
fi
rm /data/aurora/system_original_md5.file
rm /data/aurora/system_current_md5.file
sync
