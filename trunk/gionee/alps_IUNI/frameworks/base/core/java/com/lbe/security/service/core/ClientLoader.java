package com.lbe.security.service.core;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import android.util.Log;

import dalvik.system.PathClassLoader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class ClientLoader {

    private static final String LOG_TAG = "LBE-Sec";
    private static boolean DEBUG = false;

    private static final String OEM_LIBRARY_PATTERN = "/data/data/%s/app_hips";
    private static final String OEM_FILE = "oem.lock";
    private static final String CLIENT_JAR = "client.jar";

    private static Method metInitializeSandbox;
    private static boolean loaded = false;

    public static synchronized void loadLBECLient(Application targetApplication, String controllerPackageName, boolean manageSystemApps) {
        if (loaded) {
            if (DEBUG) {
                Log.w(LOG_TAG, "Duplicate LBE Client creation request!");
            }
            return;
        } else {
            loaded = true;
        }
        if (!isHipsEnabled(controllerPackageName)) {
            return;
        }
        loadLBEClassIfNeed(controllerPackageName);
        ApplicationInfo info = targetApplication.getApplicationInfo();
        try {
            if (!manageSystemApps && isSystemApp(info)) {
                if (DEBUG) {
                    Log.i(LOG_TAG, "Not manage system app " + info.packageName);
                }
                return;
            }
            metInitializeSandbox.invoke(null, controllerPackageName, OEM_LIBRARY_PATTERN, targetApplication);
        } catch (Exception e) {
            Log.d(LOG_TAG, "sandbox init fail");
            e.printStackTrace();
        }

    }

    /**
     * call this method in ZygoteInit.java preload method
     *
     * @param controllerPackageName 授权管理SDK的 Package
     */
    public static void preloadLBEClient(String controllerPackageName) {
        if (!isHipsEnabled(controllerPackageName)) {
            return;
        }
        loadLBEClassIfNeed(controllerPackageName);
    }

    private static boolean isHipsEnabled(String controllerPackageName) {
        File hipsPath = new File(String.format(OEM_LIBRARY_PATTERN, controllerPackageName));
        File oemFile = new File(hipsPath, OEM_FILE);
        if (oemFile.exists()) {
            Log.d("LBE-Sec", "oem enabled");
            return true;
        } else {
            Log.d("LBE-Sec", "oem not enabled");
            return false;
        }
    }

    private static void loadLBEClassIfNeed(String controllerPackageName) {
        if (metInitializeSandbox != null) {
            return;
        }
        loadLBEClass(controllerPackageName);
    }

    private static boolean loadLBEClass(String controllerPackageName) {
        try {
            Log.d("LBE-Sec", "try to load lbe class");
            File oemHipsPath = new File(String.format(OEM_LIBRARY_PATTERN, controllerPackageName));
            File oemClientJar = new File(oemHipsPath, CLIENT_JAR);
            if (!oemClientJar.exists()) {
                if (DEBUG) {
                    Log.w(LOG_TAG, "LBE Client file not exist, do not load lbe class");
                }
                return false;
            } else {
                if (DEBUG) {
                    Log.w(LOG_TAG, "LBE Client files all exist, load lbe class");
                }
            }

            PathClassLoader classLoader = new PathClassLoader(oemClientJar.getAbsolutePath(), oemHipsPath.getAbsolutePath(), ClassLoader.getSystemClassLoader());
            Class<?> clsClientContainer = classLoader.loadClass("com.lbe.security.service.core.client.ClientContainer");
            metInitializeSandbox = clsClientContainer.getDeclaredMethod("initializeSandbox", String.class, String.class, Application.class);
            metInitializeSandbox.setAccessible(true);
            //preload so
            Method metPreload = clsClientContainer.getDeclaredMethod("preload", String.class);
            metPreload.setAccessible(true);
            metPreload.invoke(null, String.format(OEM_LIBRARY_PATTERN, controllerPackageName));
            Log.d("LBE-Sec", "load lbe class success");
            return true;
        } catch (Exception e) {
            Log.d("LBE-Sec", "load lbe class fail");
            e.printStackTrace();
        }
        return false;
    }

    private static boolean isSystemApp(ApplicationInfo info) {
        return (info.uid < android.os.Process.FIRST_APPLICATION_UID
                || (info.flags & ApplicationInfo.FLAG_SYSTEM) != 0
                || (info.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0);
    }
}
