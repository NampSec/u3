/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#ifndef FRAME_PROCESSOR_H_
#define FRAME_PROCESSOR_H_

#include <memory.h>
#include <time.h>
#include <pthread.h>
#include <android/log.h>
#include <string.h>

#include "AlgImgDefog.h"
#include "GNCameraFeatureDefs.h"

#define LOG_TAG "FrameProcessor"

typedef enum SceneMsgTypeTag {
	SCENE_MSG_TYPE_DESTROY,
	SCENE_MSG_TYPE_CLEAR,
	SCENE_MSG_TYPE_PROC,
} SceneMsgType_t;

typedef struct MsgSenceDetectTag {
	SceneMsgType_t type;
	IMGOFFSCREEN srcImg;
	uint32_t mode;
} MsgSceneDetect_t;

class DefogListener {
public:
	virtual ~DefogListener() {};
	virtual void onCallback(int result) = 0;
};

class FrameProcessor {
private: 
	FrameProcessor();
	~FrameProcessor();
	
public:
	int32_t init();
	void 	deinit();

	int32_t registerDefogListener(DefogListener* listener);
	void 	unregisterDefogListener();
	int32_t processDefog(PIMGOFFSCREEN srcImg, bool isPreview);
	int32_t getDefogDetectionResult();
	int32_t enableFrameProcess(int32_t maxWidth, int32_t maxHeight, uint32_t format, int32_t featureMask);
	void 	disableFrameProcess(int32_t featureMask);

public:
	static FrameProcessor* getInstance();
	
private:
    static void *defogDetectionRoutine(void *data);

private:
	DefogProcessor* mDefogProcessor;
	pthread_mutex_t mDefogProcessorMutex;
	pthread_cond_t mCond ;
	pthread_mutex_t mThreadMutex ;

	pthread_t mTidDefogDetection;

    MsgSceneDetect_t mMsgScene;
	FogFlareDegree_t mFogFlareDegree;
	AlgDefogInitParm* mMemInitParm;	

	static FrameProcessor* mFrameProcessor;
    DefogListener* mDefogListener;
	bool mInitialized;
	int32_t mGNFrameProcessMask;
};
#endif /* FRAME_PROCESSOR_H_*/
