/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/
 
#include <string.h>
#include <android/log.h>
#include "GioneeDefogDetection.h"

#define LOG_TAG "GioneeDefogDetection"

namespace android {
GioneeDefogDetection::GioneeDefogDetection()
	: mListener(NULL)
	, mDefogDetectionHandler(NULL) 
	, mInitialized(false)
	, mFrameProcessor(NULL)
{
	pthread_mutex_init(&mMutex, NULL);
}

GioneeDefogDetection::~GioneeDefogDetection()
{
	pthread_mutex_destroy(&mMutex);
}

int32_t 
GioneeDefogDetection::init() 
{
	int32_t ret = 0;

	if (mFrameProcessor == NULL) {
		mFrameProcessor = FrameProcessor::getInstance();
	}

	mDefogDetectionHandler = new DefogDetectionHandler(this);
	if (mDefogDetectionHandler == NULL) {
		PRINTE("Failed to new DefogDetectionHandler.");
	}
	
	return ret;
}

void 
GioneeDefogDetection::deinit()
{	
	if (mDefogDetectionHandler != NULL) {
		delete mDefogDetectionHandler;
		mDefogDetectionHandler = NULL;
	}

	mInitialized = false;
}

int32_t 
GioneeDefogDetection::enableDefogDetection(int maxWidth, int maxHeight, uint32_t format)
{
	int32_t res = 0; 

	if (maxWidth == 0 || maxHeight == 0) {
		PRINTE("Invalid parameter.");
		return -1;
	}

    if (!mInitialized) {
		res = mFrameProcessor->enableFrameProcess(maxWidth, maxHeight, format, GN_CAMERA_FEATURE_DEFOG_DETECTION);
		if (res) {
    		PRINTE("Fail to Init Frame Processor");
			return -1;
		}

		mFrameProcessor->registerDefogListener(mDefogDetectionHandler);
    }
	
	mInitialized = true;

	return res;
}

int32_t 
GioneeDefogDetection::disableDefogDetection()
{
	int32_t res = 0; 

	if (mFrameProcessor != NULL) {
		mFrameProcessor->disableFrameProcess(GN_CAMERA_FEATURE_DEFOG_DETECTION);
		mFrameProcessor->unregisterDefogListener();
	}

	mInitialized = false;

	return res;
}

int32_t 
GioneeDefogDetection::setCameraListener(GNCameraFeatureListener* listener)
{
	int32_t res = 0; 

	pthread_mutex_lock(&mMutex);
	
	mListener = listener;
	
	pthread_mutex_unlock(&mMutex);

	return res;
}

int32_t 
GioneeDefogDetection::processDefogDetection(PIMGOFFSCREEN imgSrc)
{
	int32_t res = 0;
	
	if (!mInitialized) {
		PRINTD("processDefogDetection not Initialized");
		return -1;
	}
	
	if (mFrameProcessor != NULL) {
		res = mFrameProcessor->processDefog(imgSrc ,true);
		if (res != 0) {
			PRINTD("Failed to process defog detection.");
		}
	}

	return res;
}

void
GioneeDefogDetection::
DefogDetectionHandler::
onCallback(int result) 
{
	pthread_mutex_lock(&mGioneeDefogDetection->mMutex);
	
	if (mGioneeDefogDetection != NULL && mGioneeDefogDetection->mListener != NULL) {
		mGioneeDefogDetection->mListener->notify(GN_CAMERA_MSG_TYPE_DEFOG_DETECTION, result, 0);
	}
	
	pthread_mutex_unlock(&mGioneeDefogDetection->mMutex);
}

};
