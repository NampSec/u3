#ifndef TA3D_MOTION_H
#define TA3D_MOTION_H

#include "common.h"
#include "geometry.h"
#include "iterator.h"
#include "range.h"

namespace TouchlessA3D {
  /**
   * Represents a motion detected by the library.
   *
   * @note This class is not designed to be inherited from.
   *
   * Copyright © 2014 Crunchfish AB. All rights reserved. All information herein
   * is or may be trade secrets of Crunchfish AB.
   */
  class TA3D_EXPORT Motion {
  public:
    /**
     * The type representing an area within the motion.
     */
    typedef Rectangle<unsigned int> Area;

    /**
     * Constructs an empty motion.
     */
    Motion();

    /**
     * Copies another motion.
     *
     * @note This class uses copy semantics, not move semantics.
     */
    Motion(const Motion&);

    /**
     * Destructs the motion.
     */
    ~Motion();

    /**
     * Assigns another motion to this motion.
     *
     * @note This class uses copy semantics, not move semantics.
     */
    Motion& operator=(const Motion&);

    /**
     * Resets the motion, i.e. empties it.
     */
    void reset();

    /**
     * Checks if the motion is empty.
     *
     * @return true if the motion is empty, false otherwise.
     */
    bool isEmpty() const;

    /**
     * Adds an area where motion has been detected.
     *
     * @param area An area where motion has been detected.
     */
    void addArea(const Area& area);

    /**
     * Returns a range of areas where motion was detected.
     *
     * @return The areas where motion was detected.
     */
    const Range<Iterator<const Area> > getAreas() const;

  private:
    class Impl;
    Impl* const m_pImpl;
  };
}

#endif  // TA3D_MOTION_H
